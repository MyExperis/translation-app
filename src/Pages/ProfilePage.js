import "../CSS/ProfilePage.css"
import { useState, useEffect } from "react";
import { deleteList, getUserbyName } from "../userApi";
import { useHistory } from 'react-router-dom';
import withAuth from '../HOC/withAuth';
import HeaderComponent from "../Components/HeaderComponent";

//The profile page that shows the profile, last translated texts and has buttons 
//for deletion and logout
function ProfilePage() {
    const [userState, setUserState] = useState({
        name: "",
        translations: []
    });

    useEffect(() => {
        setUserState(JSON.parse(localStorage.getItem("currentUser")))
    }, []);

    const data = userState.name;

    //Limits the visible translations to only the last 10.
    let publicTranslations = userState.translations.slice(-10, userState.translations.length);

    //Maps the translations to list items in the div.
    const translationListRender = publicTranslations.map((t, index) => <li key={index}>{t}</li>);


    //Deletes the users translations from the database
    //First gets the user to get acces to their id and use it for the next query to the database.
    async function deleteTranslationList() {
        let userID = 0;
        let user = {};
        getUserbyName(userState.name)
            .then((response) => {
                userID = response[0].id;
                deleteList(userID)
                    .then((response => {
                        setUserState({ name: response.name, translations: response.translations });
                        user = { name: response.name, translations: response.translations };
                        localStorage.setItem("currentUser", JSON.stringify(user));
                    }))
            });
    }

    const history = useHistory();

    //logs out the user by clearing the local storage of their data and token
    function logout() {
        localStorage.clear();
        history.push("/");
    }

    return (
        <div className="profileWrapper">
            <HeaderComponent dataParentToChild={data}/>
            <div className="profileTranslationField">
                <ul className="translationList">
                    {translationListRender}
                </ul>
            </div>
            <button className="deleteTranslationBtn" onClick={deleteTranslationList}>DELETE TRANSLATIONS</button>
            <button className="logoutBtn" onClick={logout}>LOGOUT</button>
        </div>
    )
}

export default withAuth(ProfilePage);