import '../CSS/ProfileComponent.css';
import avatar from '../Resources/avatar.png';
import { useHistory } from 'react-router-dom';

//the profile div sued in profile and translate page.
function ProfileComponent({ dataParentToChild }) {

    const history = useHistory();

    //gets the username by props so that it can lead the user to their profilepage.
    function click() {
        history.push("/profile/" + dataParentToChild);
    }

    return (
        <div className="profileComp">
            <div className="iconWrap">
                <img className="avatar" src={avatar} alt="avatar" onClick={click} />
            </div>
            <h1>{dataParentToChild}</h1>
        </div>
    )

}

export default ProfileComponent;