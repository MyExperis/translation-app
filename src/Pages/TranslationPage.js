import '../CSS/TranslationPage.css'
import { useState, useEffect } from "react";
import withAuth from '../HOC/withAuth';
import { addTranslation, getUserbyName } from "../userApi";
import HeaderComponent from "../Components/HeaderComponent";

//Displays the translate page that shows the 
//profile, translation result field, input for text to be translated
function TranslationPage() {
    const [userState, setUserState] = useState({
        name: "",
        translations: []
    });
    const [inputState, setInputState] = useState("");
    const [translateState, setTranslateState] = useState("");

    useEffect(() => {
        setUserState(JSON.parse(localStorage.getItem("currentUser")))
    }, []);

    const data = userState.name;

    //Adds the translation to the database
    //First gets the user to get access to their id and use it in the next query to append their translations.
    //Then sets the string ready to get translated by replacing " " with "_" to receive spaces in the translated text.
    function translate() {
        let userID;
        getUserbyName(userState.name)
            .then((response) => {
                userID = response[0].id;
                let translations = response[0].translations;
                translations.push(inputState);

                let user = { name: userState.name, translations: translations }
                localStorage.setItem("currentUser", JSON.stringify(user));

                addTranslation(translations, userID);
                let replaceSpaceStr = inputState.replaceAll(" ", "_");
                setTranslateState(replaceSpaceStr);
            })
    }

    //Validates the input by limiting the user and not accepting special chars or numbers and updates the ui accordingly.
    const handleChange = (event) => {
        let value = event.target.value.replace(/[^A-Za-z\s]/ig, '')
        setInputState(value);
        event.target.value = value;
    }

    //Maps the translated text with the respective sign images and displays it in the div and ui.
    function RenderTranslations() {
        let translateTest = translateState.split("");
        return translateTest.map((element, index) => <img src={`/assets/individial_signs/${element}.png`} alt={""} key={index} />) //MAYBE ADD PNG ENDING
    }

    return (
        <div className="translationPageWrapper">
             <HeaderComponent dataParentToChild={data}/>
            <div className="translateInputField" id="inputTranslateField">
                    <input id="translateInput" type="text" placeholder="Enter text to be translated" onChange={handleChange}>
                    </input>
                <button className="translationTranslateBtn" onClick={translate}>TRANSLATE</button>
            </div>
            <div className="translationContainer">
                <div className="translationTitle">TRANSLATION</div>
                <div className="translationField">
                    <RenderTranslations></RenderTranslations>
                </div>
            </div>
        </div>
    );

}

export default withAuth(TranslationPage);