import { Redirect } from "react-router-dom"

function withAuthLogin(Component) {

    return function () {

        //check if allowed
        if (localStorage.getItem('token') === 'valid') {
            return <Redirect to="/translation" />
        }
        else {
            return <Component />
        }

    }
}

export default withAuthLogin
