const USER_API_URL = 'http://localhost:3005/users';

//GET all books
export const getAllUsers = () => {
    return fetch(USER_API_URL)
        .then(response => response.json())
        .then(response => response)
}

//GET specific user by id
export const getUserbyName = async (username) => {
    const response = await fetch(`${USER_API_URL}?name=${username}`);
    const actualRes = await response.json();
    return actualRes;
}

//POST new user
export const postUser = (username) => {
    try {
        return fetch(USER_API_URL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ name: username, translations: [] })
        })
            .then(async (response) => {
                if (!response.ok) {
                    const { error = "Unknown error" } = await response.json();
                    throw new Error(error);
                }
                return response.json();
            })
    } catch (error) {
        console.log("error")
        console.error(error)
    }

}

//Delete users translations by PATCH
export const deleteList = (id) => {
    try {
        console.log("api: " + id);
        return fetch(`${USER_API_URL}/${id}`, {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ translations: [] })
        })
            .then(async (response) => {
                if (!response.ok) {
                    const { error = "Unknown error" } = await response.json();
                    throw new Error(error);
                }
                return response.json();
            })

    } catch (error) {
        console.log("error")
        console.error(error)
    }
}

//PATCH to add new translation to specific user by id
export const addTranslation = (translation, id) => {
    try {
        return fetch(`${USER_API_URL}/${id}`, {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ translations: translation })
        })
            .then(async (response) => {
                if (!response.ok) {
                    const { error = "Unknown error" } = await response.json();
                    throw new Error(error);
                }
                return response.json();
            })

    } catch (error) {
        console.log("error")
        console.error(error)
    }
}





