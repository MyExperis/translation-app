import './CSS/App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import StartPage from './Pages/StartPage';
import ProfilePage from './Pages/ProfilePage';
import TranslationPage from './Pages/TranslationPage';
import FooterComponent from './Components/FooterComponent';

function App() {

  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={StartPage} />
          <Route path="/translation" component={TranslationPage} />
          <Route path="/profile/:name" component={ProfilePage} />
          <Route path="*" component={StartPage}/>
        </Switch>
      </BrowserRouter>
      <FooterComponent/>
    </div>
  );
}

export default App;
