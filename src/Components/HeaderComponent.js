import ProfileComponent from "./ProfileComponent";
import profileImage from '../Resources/profileImage.png';
import "../CSS/HeaderComponent.css"

function HeaderComponent({ dataParentToChild }) {

    const data = dataParentToChild;


    return (
        <div className="Header">
            <div className="titleDiv">
                <div className="title">Lost in translation
                </div>
                <img className="titleSignImage" src={profileImage} alt="sign language hand" />
            </div>
            <ProfileComponent dataParentToChild={data} />
        </div>
    );
}



export default HeaderComponent;