import '../CSS/StartPage.css';
import { React, useState, useEffect } from 'react';
import startImage from '../Resources/startImage.png';
import { useHistory } from 'react-router-dom';
import { getUserbyName, postUser } from "../userApi";
import withAuthLogin from '../HOC/withAuthLogin';
//import FooterComponent from '../Components/FooterComponent';

//The applications start page that lets the user either login or register as a new user.
//A user who logs out oand doesn't have a valid token gets transferred back here
function StartPage() {

    const [userState, setUserState] = useState({
        username: "",
        userTranslation: []
    });

    const history = useHistory();

    //Checks if the username already exists, if yes then logs in. if not, creates a new user. 
    async function checkUser() {
        getUserbyName(userState.username)
            .then((response) => {

                if (response.length > 0) {
                    const user = { name: response[0].name, translations: response[0].translations };
                    localStorage.setItem("currentUser", JSON.stringify(user));
                    localStorage.setItem("token", "valid");
                    history.push("/translation");
                } else {
                    alert("You are now registered, please enter your name again to login.")
                    postUser(userState.username);
                }

            })
    }

    useEffect(() => {
        localStorage.getItem('token', 'invalid')
    })

    //saves the input in the state
    const handleChange = (event) => {
        let username = event.target.value;
        setUserState({ username });
    }

    return (
        <div>
        
        <div className="startPageWrapper">
            <div className="startTitleWrapper">
                <h1 className="startTitle">Lost In Translation</h1>
            </div>

            <div className="startInputField" id="inputUsernameField">
                <input id="userNameField" type="text" placeholder="Enter your name" onChange={handleChange}></input>
                <button onClick={checkUser}>LOGIN</button>
            </div>
            <img className="startPageImage" src={startImage} alt="three sign language hands" />
        </div >
       
        </div>
    );
}

export default withAuthLogin(StartPage);