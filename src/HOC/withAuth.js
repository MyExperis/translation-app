import { Redirect } from "react-router-dom"

//checks the token if the user is allowed entrance to the different pages.
function withAuth(Component) {

    return function () {

        //check if allowed
        if (localStorage.getItem('token') === 'valid') {
            return <Component />
        }
        else {
            return <Redirect to="/" />
        }

    }
}

export default withAuth
